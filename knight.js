const Piece = require('./piece');

class Knight extends Piece {
  constructor(color) {
    super(color);
  }

  isValidMove(fromX, fromY, toX, toY) {
    if (fromX+1 == toX && fromY+2 == toY || // 1x right 2y up
      fromX+2 == toX && fromY+1 == toY ||   // 2x right 1y up  
      fromX+2 == toX && fromY-1 == toY ||   // 2x right 1y down 
      fromX+1 == toX && fromY-2 == toY ||   // 1x right 2y down 
      fromX-1 == toX && fromY-2 == toY ||   // 1x left 2y down 
      fromX-2 == toX && fromY-1 == toY ||   // 2x left 1y down 
      fromX-2 == toX && fromY+1 == toY ||   // 2x left 1y up 
      fromX-1 == toX && fromY+2 == toY      // 1x left 2y up 
    ) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return this._color + 'k';  
  }

};

module.exports = Knight;

