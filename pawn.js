const Piece = require('./piece');

class Pawn extends Piece {
  constructor(color) {
    super(color);
  }

  isValidMove(fromX, fromY, toX, toY) {
    const direction = this._color == this.constructor.WHITE ? 1 : -1;
    const numSquares = this._hasMoved ? 1 : 2;
    if (this._hasMoved) {
      if (fromX == toX && (fromY + 1 * direction) == toY) {
        return true;
      } else {
        return false;
      }
    } else {
      if (fromX == toX && (fromY + 1 * direction == toY || fromY + 2 * direction == toY)) {
        return true;
      } else {
        return false;
      }
    }
  }

  // Specific for pawns. Return true is the destination is 1 square diagonally
  // to both sides on the direction of the pawn movement.
  isValidCaptureMove(fromX, fromY, toX, toY) {
      const direction = this._color == this.constructor.WHITE ? 1 : -1;
    if (((fromX + 1 * direction) == toX || (fromX - 1 * direction) == toX) && (fromY + 1 * direction) == toY) {
        return true;
      } else {
        return false;
      }
  }

  render() {
    return this._color + 'P';  
  }
};

module.exports = Pawn;
