const Piece = require('./piece');
const diagonalMoverMixin = require('./diagonal-mover-mixin');

class Bishop extends Piece {
  constructor(color) {
    super(color);
  }

  isValidMove(...params) {
    return this.isValidMoveDiagonal(...params);
  }

  getPotentialBlockingPositions(...params) {
    return this.getPotentialBlockingPositionsDiagonal(...params);
  }

  render() {
    return this._color + 'B';  
  }
};

// Mixins
Object.assign(Bishop.prototype, diagonalMoverMixin);

module.exports = Bishop;
