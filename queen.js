const Piece = require('./piece');
const straightMoverMixin = require('./straight-mover-mixin');
const diagonalMoverMixin = require('./diagonal-mover-mixin');

class Queen extends Piece {
  constructor(color) {
    super(color);
  }

  isValidMove(...params) {
    return this.isValidMoveStraight(...params) || this.isValidMoveDiagonal(...params);
  }

  getPotentialBlockingPositions(...params) {
    return this.getPotentialBlockingPositionsStraight(...params).concat(this.getPotentialBlockingPositionsDiagonal(...params));
  }

  render() {
    return this._color + 'Q';  
  }
}

// Mixins
Object.assign(Queen.prototype, straightMoverMixin);
Object.assign(Queen.prototype, diagonalMoverMixin);

module.exports = Queen;
