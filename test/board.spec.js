// Import chai.
const should = require('chai').should();
const path = require('path');

// Import Board class.
const Board = require(path.join(__dirname, '..', 'board'));
const Piece = require(path.join(__dirname, '..', 'piece'));
const ValidationError = require(path.join(__dirname, '..', 'validation-error'));

describe('Board', () => {
  describe('#checkValidFromFileRank', () => {
    let board;

    beforeEach(() => {
      board = new Board();
    });

    it('Should not accept an empty string', () => {
      (() => board.checkValidFromFileRank('', Piece.WHITE))
        .should.throw(ValidationError).with.property('code', ValidationError.INVALID_FILE_RANK_CODE);
    });

     it('Should not accept a string with only one character', () => {
       (() => board.checkValidFromFileRank('A', Piece.WHITE))
         .should.throw(ValidationError).with.property('code', ValidationError.INVALID_FILE_RANK_CODE);
     });

    it('Should accept valid File/Rank strings with spaces at the beginning and/or end', () => {
      board.checkValidFromFileRank('  A1  ', Piece.WHITE).should.be.true;
    });

    it('Should allow Pawns to move 2 squares in their first move', () => {
      board.movePiece('A2', 'A4', Piece.WHITE).should.be.true;
      board.movePiece('A7', 'A5', Piece.BLACK).should.be.true;
    });

    it('Should not allow Pawns to move 2 squares after their first move', () => {
      board.movePiece('A2', 'A4', Piece.WHITE).should.be.true;
      board.movePiece('B7', 'B5', Piece.BLACK).should.be.true;
      (() => board.movePiece('A4', 'A6', Piece.WHITE)).should
        .throw(ValidationError).with.property('code', ValidationError.INVALID_PIECE_MOVE_CODE);
    });
  });
});
