const Piece = require('./piece');
const straightMoverMixin = require('./straight-mover-mixin');

class Rook extends Piece {
  constructor(color) {
    super(color);
  }

  isValidMove(...params) {
    return this.isValidMoveStraight(...params);
  }

  getPotentialBlockingPositions(...params) {
    return this.getPotentialBlockingPositionsStraight(...params);
  }

  render() {
    return this._color + 'R';  
  }
};

// Mixins
Object.assign(Rook.prototype, straightMoverMixin);


module.exports = Rook;
