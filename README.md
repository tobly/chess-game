# README #

Basic object-oriented chess game for the command line implemented in Node.js.

### How do I get set up? ###

Just clone and run:
`$npm start`

To test:
`$npm test`

