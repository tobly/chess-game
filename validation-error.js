class ValidationError extends Error {
  constructor(code, msgParam, ...params) {
    super(...params);

    this._code = code;
    this._message = this.constructor._MESSAGES[code](msgParam);
  }

  /**********
   * Public *
   **********/
  static get INVALID_FILE_RANK_CODE() { return 1; }
  static get EMPTY_SQUARE_CODE() { return 2; }
  static get WRONG_PIECE_COLOR_CODE() { return 3; }
  static get INVALID_PIECE_MOVE_CODE() { return 4; }
  static get CANT_MOVE_TO_SAME_TEAM_PIECE_SQUARE_CODE() { return 5; }
  static get BLOCKING_PIECES_CODE() { return 6; }
  static get PAWN_CANT_MOVE_DIAGONALLY_CODE() { return 7; }

  get code() { return this._code; }
  get message() { return this._message; }

  /***********
   * Private *
   ***********/

  static get _MESSAGES() {
    let messages = {};
    messages[this.INVALID_FILE_RANK_CODE] = this._renderInvalidFileRankValueMsg;
    messages[this.EMPTY_SQUARE_CODE] = this._renderEmptySquareMsg;
    messages[this.WRONG_PIECE_COLOR_CODE] = this._renderWrongPieceColorMsg;
    messages[this.INVALID_PIECE_MOVE_CODE] = this._renderInvalidPieceMoveMsg;
    messages[this.CANT_MOVE_TO_SAME_TEAM_PIECE_SQUARE_CODE] = this._renderCantMoveToSameTeamPieceSquareMsg;
    messages[this.BLOCKING_PIECES_CODE] = this._renderBlockingPiecesMsg;
    messages[this.PAWN_CANT_MOVE_DIAGONALLY_CODE] = this._renderPawnCantMoveDiagonallyMsg;
    return messages;
  }
  
  static _renderInvalidFileRankValueMsg() {
    return 'Invalid position. It must be in the following format: a letter (file) from A to H followed by a Number from 1 to 8 (rank).';
  }

  static _renderEmptySquareMsg(colorTxt) {
    return `Empty square selected. Select a square that contains a ${colorTxt} piece.`
  }

  static _renderWrongPieceColorMsg(letter) {
    return `Wrong piece color selected you should choose one that starts with the letter "${letter}".`; 
  }

  static _renderInvalidPieceMoveMsg(piece) {
    return `Invalid ${piece.constructor.name.toLowerCase()} move.`; 
  }

  static _renderCantMoveToSameTeamPieceSquareMsg() {
    return `Can't move to a square occupied by one of your pieces.`; 
  }

  static _renderBlockingPiecesMsg() {
    return `There are pieces blocking the movement.`; 
  }

  static _renderPawnCantMoveDiagonallyMsg() {
    return `Pawns can move diagonally only if there is an enemy piece to capture.`; 
  }
}

module.exports = ValidationError;
