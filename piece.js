class Piece {
  constructor(color) {
    this._color = color;
    // Should be set to true when moved for the first time.
    this._hasMoved = false;
  }

  // Constants
  static get WHITE() { return 'W';} 
  static get BLACK() { return 'B';} 

  // Getters/Setters
  get color() { return this._color; }

  set hasMoved(value) {
    this._hasMoved = value;
  }

  isValidMove(fromX, fromY, toX, toY) {
    // Abstract function. Just a reminder that it must be implemented.
  }
};

module.exports = Piece;
