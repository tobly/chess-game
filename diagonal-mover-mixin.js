// Mixing to reuse the rules of multiple square horizontal/vertical movers.
module.exports = {
  isDiagonalMover: true,

  isValidMoveDiagonal(fromX, fromY, toX, toY) {
    // It has to move the same number of squares for x and y.
    if (Math.abs(toX-fromX) == Math.abs(toY-fromY)) {
      return true;
    } else {
      return false;
    }
  },

  // Returns an array of objects with square positions (e.g. [{x: 1, y: 6}, ..])
  // that, if occupied, should prevent the move.
  getPotentialBlockingPositionsDiagonal(fromX, fromY, toX, toY) {
    const distanceX = toX - fromX;
    const distanceY = toY - fromY;
    const absDistance = Math.abs(distanceX);

    const positions = [];
    for (let i=absDistance-1; i>0; i--) {
      positions.push({
        x: fromX + i * (distanceX < 0 ? -1 : 1),
        y: fromY + i * (distanceY < 0 ? -1 : 1)
      });
    }

    return positions;
  }
}
