const ValidationError = require('./validation-error');

const Piece = require('./piece');
const Bishop = require('./bishop');
const King = require('./king');
const Knight = require('./knight');
const Pawn = require('./pawn');
const Queen = require('./queen');
const Rook = require('./rook');

class Board {
  constructor() {
    this._clearBoard();
    this._initializePieces();
  }

  /**********
   * Public *
   **********/

  // Returns true if valid file and rank string and a ValidationError if not valid.
  checkValidFromFileRank(fileRank, color) {
    fileRank = fileRank.trim();

    this._checkValidFileRank(fileRank); 

    const [x, y] = this._convertFileRankToPosition(fileRank);

    // Checks that the "from" square contains a piece.
    if (this._board[x][y] === null) {
      throw new ValidationError(ValidationError.EMPTY_SQUARE_CODE, Piece.WHITE ? 'white' : 'black');
    }

    // Checks that the "from" square piece is the right color.
    if (this._board[x][y].color != color) {
      throw new ValidationError(ValidationError.WRONG_PIECE_COLOR_CODE, color == Piece.WHITE ? 'W' : 'B');
    }

    return true;
  }

  checkValidFromAndToFileRanks(fromFileRank, toFileRank, color) {
    this.checkValidFromFileRank(fromFileRank, color);

    this._checkValidFileRank(fromFileRank);
    const [fromX, fromY] = this._convertFileRankToPosition(fromFileRank);
    this._checkValidFileRank(toFileRank);
    const [toX, toY] = this._convertFileRankToPosition(toFileRank);
     
    if (this._checkIndependentRules(fromX, fromY, toX, toY) ||
        this._checkRegularRules(fromX, fromY, toX, toY)) {
      return true;
    }
  }
  
  movePiece(fromFileRank, toFileRank, color) {
    this.checkValidFromAndToFileRanks(fromFileRank, toFileRank, color);

    const [fromX, fromY] =  this._convertFileRankToPosition(fromFileRank);
    const [toX, toY] =  this._convertFileRankToPosition(toFileRank);
    this._board[toX][toY] = this._board[fromX][fromY];
    this._board[fromX][fromY] = null;
    this._board[toX][toY].hasMoved = true;

    return true;
  }

  render() {
    for(let y=this.constructor._BOARD_SIZE-1; y>=0; y--) {
      let rankString = y+1;
      let piece;
      for(let x=0; x<this.constructor._BOARD_SIZE; x++) {
        piece = this._board[x][y];
        rankString += ' ' + (piece ? piece.render() : '--');  
      }
      console.log(rankString);
    }
    let fileTagsRow = '  ';
    this.constructor._FILES.forEach(rank => fileTagsRow += rank + '  ');
    console.log(fileTagsRow);
  }

  /***********
   * Private *
   ***********/

  static get _BOARD_SIZE() { return 8; }
  static get _FILES() { return ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H']; }

  // Pieces initial positions
  static _PAWNS_INITIAL_POSITIONS(color) {
    const pawnInitialPositions = [];
    for(let i=0; i<8; i++) {
      pawnInitialPositions.push([Pawn, i, color == Piece.WHITE ? 1 : 6]);
    }
    return pawnInitialPositions;
  }
  static _KING_INITIAL_POSITION(color) { return [King, 4, color == Piece.WHITE ? 0 : 7]; }
  static _QUEEN_INITIAL_POSITION(color) { return [Queen, 3, color == Piece.WHITE ? 0 : 7]; }
  static _BISHOPS_INITIAL_POSITIONS(color) { return [[Bishop, 2, color == Piece.WHITE ? 0 : 7], [Bishop, 5, color == Piece.WHITE ? 0 : 7]]; }
  static _KNIGHTS_INITIAL_POSITIONS(color) { return [[Knight, 1, color == Piece.WHITE ? 0 : 7], [Knight, 6, color == Piece.WHITE ? 0 : 7]]; }
  static _ROOKS_INITIAL_POSITIONS(color) { return [[Rook, 0, color == Piece.WHITE ? 0 : 7], [Rook, 7, color == Piece.WHITE ? 0 : 7]]; }
  static _GET_PIECES_INITIAL_POSITIONS(color) {
    return this._PAWNS_INITIAL_POSITIONS(color)
      .concat([this._KING_INITIAL_POSITION(color)])
      .concat([this._QUEEN_INITIAL_POSITION(color)])
      .concat(this._BISHOPS_INITIAL_POSITIONS(color))
      .concat(this._KNIGHTS_INITIAL_POSITIONS(color))
      .concat(this._ROOKS_INITIAL_POSITIONS(color));
  }

  _clearBoard() {
    this._board = new Array(this.constructor._BOARD_SIZE);
    for (let i=0; i<this.constructor._BOARD_SIZE; i++) {
      this._board[i] = new Array(this.constructor._BOARD_SIZE);
      this._board[i].fill(null);
    };
  }

  _initializePieces() { 
    // White pieces
    this.constructor._GET_PIECES_INITIAL_POSITIONS(Piece.WHITE).forEach(pos => {
      this._board[pos[1]][pos[2]] = new pos[0](Piece.WHITE);
    });
    // Black Pieces
    this.constructor._GET_PIECES_INITIAL_POSITIONS(Piece.BLACK).forEach(pos => {
      this._board[pos[1]][pos[2]] = new pos[0](Piece.BLACK);
    });
  }

  // If a file and rank literal is valid returns true.
  // Throws a ValidationError otherwise.
  _checkValidFileRank(fileRank) {
    const file = fileRank[0] && fileRank[0].toUpperCase();
    const rank = fileRank[1];

    // Checks if file and rank values are valid
    if (!/[A-Z]/.test(file) || !/[1-8]/.test(rank)) {
      throw new ValidationError(ValidationError.INVALID_FILE_RANK_CODE);
    } 

    return true;
  }

  // If passed don't need to pass any other check.
  _checkIndependentRules(fromX, fromY, toX, toY) {
    const fromPiece = this._board[fromX][fromY];
    if (fromPiece instanceof Pawn && fromPiece.isValidCaptureMove(fromX, fromY, toX, toY)) {
      if (!this._board[toX][toY] || this._board[toX][toY].color == fromPiece.color) {
        throw new ValidationError(ValidationError.PAWN_CANT_MOVE_DIAGONALLY_CODE);
      }
      return true;
    }
    return false;
  }

  // Series of checks
  _checkRegularRules(fromX, fromY, toX, toY) {
    const fromPiece = this._board[fromX][fromY];

    // Checks if valid move.
    if (!fromPiece.isValidMove(fromX, fromY, toX, toY)) {
      throw new ValidationError(ValidationError.INVALID_PIECE_MOVE_CODE, fromPiece);
    }

    // Checks if trying to move to a square occupied by a piece of it's own team.
    if (this._board[toX][toY] && this._board[toX][toY].color == fromPiece.color) {
      throw new ValidationError(ValidationError.CANT_MOVE_TO_SAME_TEAM_PIECE_SQUARE_CODE);
    }

    // Checks if pieces are blocking moves.
    if (fromPiece.getPotentialBlockingPositions) {
      fromPiece.getPotentialBlockingPositions(fromX, fromY, toX, toY).forEach(pos => {
        if (this._board[pos.x][pos.y]) {
          throw new ValidationError(ValidationError.BLOCKING_PIECES_CODE);
        }
      });
    };

    return true; 
  }

  _convertFileRankToPosition(fileRank) {
    return [this._convertFileToX(fileRank[0]), this._convertRankToY(fileRank[1])];
  }

  _convertFileToX(file) {
    return file.toUpperCase().charCodeAt(0) - 'A'.charCodeAt(0);
  }

  _convertRankToY(rank) {
    return rank.charCodeAt(0) - '1'.charCodeAt(0);
  }

};

module.exports = Board;
