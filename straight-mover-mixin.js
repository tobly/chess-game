// Mixing to reuse the rules of multiple square horizontal/vertical movers.
module.exports = {
  isStraightMover: true,

  isValidMoveStraight(fromX, fromY, toX, toY) {
    const absDistanceX = Math.abs(toX-fromX); 
    const absDistanceY = Math.abs(toY-fromY); 
    // The distance must be 0 in one axis and > 1 in the other
    if ((absDistanceX > 0 && absDistanceY == 0) ||
        (absDistanceY > 0 && absDistanceX == 0)) {
      return true;
    } else {
      return false;
    }
  },

  // Returns an array of objects with square coodinates (e.g. [{x: 1, y: 6}, ..])
  // that, if occupied, should prevent the move.
  getPotentialBlockingPositionsStraight(fromX, fromY, toX, toY) {
    const distanceX = toX - fromX;
    const distanceY = toY - fromY;
    const absDistance = Math.abs(distanceX) > 0 ? Math.abs(distanceX) : Math.abs(distanceY);

    const positions = [];
    for (let i=absDistance-1; i>0; i--) {
      positions.push({
        x: fromX + (distanceX == 0 ? 0 : (i * (distanceX < 0 ? -1 : 1))),
        y: fromY + (distanceY == 0 ? 0 : (i * (distanceY < 0 ? -1 : 1)))
      });
    }

    return positions;
  }
}
