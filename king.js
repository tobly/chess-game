const Piece = require('./piece');

class King extends Piece {
  constructor(color) {
    super(color);
  }

  isValidMove(fromX, fromY, toX, toY) {
    if (fromX == toX && fromY+1 == toY ||   // y up
      fromX+1 == toX && fromY+1 == toY ||   // x right y up  
      fromX+1 == toX && fromY == toY ||     // x right 
      fromX+1 == toX && fromY-1 == toY ||   // x right y down 
      fromX == toX && fromY-1 == toY ||     // y down 
      fromX-1 == toX && fromY-1 == toY ||   // x left y down 
      fromX-1 == toX && fromY == toY ||     // x left 
      fromX-1 == toX && fromY+1 == toY      // x left y up 
    ) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return this._color + 'K';  
  }

};

module.exports = King;
