const readline = require('readline-promise').default;

const Board = require('./board');
const Piece = require('./piece');
const ValidationError = require('./validation-error');

class ChessGame {
  constructor() {
    this._rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
  }

  /**********
   * Public *
   **********/
  start() {
    this._board = new Board();
    // Current turn color
    this._color = Piece.WHITE;

    this._startUserInteracion();
  }

  /***********
   * Private *
   ***********/
  _startUserInteracion() {
    this._showWelcomeMessage();
    this._startNewTurn();
  }

  _startNewTurn() {
    console.log("\n" + (this._color == Piece.WHITE ? 'White' : 'Black') + " player's turn");
    this._board.render();

    this._readPositionsAndMove();
  }

  _readPositionsAndMove() {
    this._readPositions().then(([fromFileRank, toFileRank]) => {
      this._board.movePiece(fromFileRank, toFileRank, this._color);
      this._toggleColor();
      this._startNewTurn();
    }).catch(e => {
        if (e instanceof ValidationError) {
          console.log(e.message);
          this._readPositionsAndMove();
        } else {
          console.log('An unexpected error occurred!!');
          console.log(e);
          process.exit(1);
        }
    });
  }

  _readPositions() {
    let fromFileRank;
    return this._readFromPosition().then(fileRank => {

      fromFileRank = fileRank;
      return this._readToPosition(fromFileRank);
    }).then(toFileRank => {
      return [fromFileRank, toFileRank];
    });
  }

  _readFromPosition() {
    return this._rl.questionAsync('Select one of your pieces (e.g. B2 or b2)? ').then(fileRank => {
      if (this._board.checkValidFromFileRank(fileRank, this._color)) return fileRank;
    });
  }

  _readToPosition(fromFileRank) {
    return this._rl.questionAsync('Were do you want to move it to (e.g. E3 or e3)? ').then(toFileRank => {
      if (this._board.checkValidFromAndToFileRanks(fromFileRank, toFileRank, this._color)) return toFileRank;
    });
  }

  _toggleColor() {
    // Storing color info in Piece class to avoid circular dependencies. Piece
    // is a more appropriate information expert and it makes much more sense to
    // refer it from this class than the other way around.
    if (!this._color) {
      this._color = Piece.WHITE;
    } else {
      this._color = this._color == Piece.WHITE ? Piece.BLACK : Piece.WHITE;
    }
  }

  _showWelcomeMessage() {
    const welcomeMsg = 'Welcome!!\n' +
      'Follow the instructions as they come. Write "exit" at any point to leave the game.\n';
    console.log(welcomeMsg);
  }
};

const game = new ChessGame();
game.start();
